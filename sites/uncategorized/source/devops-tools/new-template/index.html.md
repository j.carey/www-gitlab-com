---
layout: markdown_page
title: "New Template"
---
<!-- This is the template for defining sections which will be included in a tool comparison page. This .md file is included in the top of the page and a table of feature comparisons is added directly below it. This template provides the sections which can be included, and the order to include them. If a section has no content yet then leave it out. Leave this note in tact so that others can see where new sections should be added.

## Summary
   - minimal requirement <-- comment. delete this line
## Strengths
## Weaknesses
## Who buys and why
## Comments/Anecdotes
   - possible customer issues with product  <-- comment. delete this line
   - sample benefits and success stories  <-- comment. delete this line
   - date, source, insight  <-- comment. delete this line
## Resources
   - links to communities, etc  <-- comment. delete this line
   - bulleted list  <-- comment. delete this line
## FAQs
 - about the product  <-- comment. delete this line
## Integrations
## Pricing
   - summary, links to tool website  <-- comment. delete this line
### Value/ROI
   - link to ROI calc?  <-- comment. delete this line
## Questions to ask
   - positioning questions, traps, etc.  <-- comment. delete this line
## Comparison
   - features comparison table will follow this <-- comment. delete this line

<!------------------Begin page additions below this line ------------------ -->

## On this page
{:.no_toc}

- TOC
{:toc}

## Summary

### Competitor Name
Competitor's Application monitors, analyzes and manages applications and IT infrastructure. It monitors and analyzes application performance, issues, root causes, and outages to improve user experience and stability.  It allows DevOps teams to monitor performance, availability and response times of applications allowing them to identify, isolate and diagnose any existing issues or potential problems.
      
### Gitlab
GitLab has a powerful monitoring capability with Prometheus, a time-series monitoring service, providing a flexible platform for monitoring GitLab and other software products. GitLab provides out of the box monitoring with Prometheus, providing easy access to high quality time-series monitoring of GitLab services.  GitLab has built-in monitoring that can automatically monitor your deployed applications, with extensive support for native cloud, container and microservices technology.  Additionally, Gitlab uses Jaeger, an open source end-to-end distributed tracing system used for monitoring and troubleshooting microservices-based distributed systems.

## Use Cases
The 2 key use cases provided by Competitor are:

    - Source Code Management (SCM)
    - Continuous Integration (CI)
    
GitLab provides an end to end DevOps platform covering 8 use cases from Requirements Management through Monitoring.  A complete list can be found [here](https://about.gitlab.com/handbook/use-cases/).


### Source Code Management (SCM)
Introductory paragraph about the use case.  

The use case is broken into 3-4 key capabilities.  This anchors us at the right level to do comparisons in language that customers will easily understand and also gives us the ability to summarize our differentiators in a way that compels prospect to take next steps.

    - Capability 1:  Brief overview of the capability described in vendor-neutral terms.
    - Capability 2:  Brief overview of the capability described in vendor-neutral terms. 
    - Capability 3:
    - Capability 4: 



#### Capability Comparison

Capability       |  GitLab                       | Competitor                                   
---------------- | ----------------------------- | -----------------------------
Capability 1     | Brief description of how GitLab delivers capability <ul><li>Link to Demo</li><li>Link to Analysis by GitLab </li></ul> | Brief description of how competitor delivers capability <ul><li>Link to External Demo</li><li>Link to External Analysis</li></ul> 
Capability 2     |      |
Capability 3     |      |
Capability 4     |      |

####  Top 5 Differentiating Features in SCM

Feature          |  GitLab                 | Competitor            
---------------- | ----------------------- | ----------------------
Feature 1     | Describe feature in GitLab     |  Describe how competitor delivers
Feature 2     |      | 
Feature 3     |      | 
Feature 4     |      | 
Feature 5     |      | 

For a detailed feature by feature comparison go [here](#FeatureComp)


####  Testimonials

    - Case Study 1
    - Case Study 2
    - Analyst Report
    - G2/Trust Radius Reviews



### Continuous Integration (CI)

Introductory paragraph about the use case.  

The use case is broken into 3-4 key capabilities.  This anchors us at the right level to do comparisons in language that customers will easily understand and also gives us the ability to summarize our differentiators in a way that compels prospect to take next steps.

    - Capability 1:  Brief overview of the capability described in vendor-neutral terms.
    - Capability 2:  Brief overview of the capability described in vendor-neutral terms. 
    - Capability 3:
    - Capability 4: 



#### Capability Comparison

Capability       |  GitLab                       | Competitor                                   
---------------- | ----------------------------- | -----------------------------
Capability 1     | Brief description of how GitLab delivers capability <ul><li>Link to Demo</li><li>Link to Analysis by GitLab </li></ul> | Brief description of how competitor delivers capability <ul><li>Link to External Demo</li><li>Link to External Analysis</li></ul> 
Capability 2     |      |
Capability 3     |      |
Capability 4     |      |

####  Top 5 Differentiating Features in SCM

Feature          |  GitLab                 | Competitor            
---------------- | ----------------------- | ----------------------
Feature 1     | Describe feature in GitLab     |  Describe how competitor delivers
Feature 2     |      | 
Feature 3     |      | 
Feature 4     |      | 
Feature 5     |      | 

For a detailed feature by feature comparison go [here](#FeatureComp)


####  Testimonials

    - Case Study 1
    - Case Study 2
    - Analyst Report
    - G2/Trust Radius Reviews


## Feature Comparison <a name="FeatureComp"></a>


Description          |  GitLab                 | Competitor            
---------------- | ----------------------- | ----------------------
Feature 1     | Y/N Check Box     |  Y/N Check Box
Feature 2     | Y/N Check Box     |  Y/N Check Box
Feature 3     | Y/N Check Box     |  Y/N Check Box 
Feature 4     | Y/N Check Box     |  Y/N Check Box 
Feature 5     | Y/N Check Box     |  Y/N Check Box 
